CLIENT_MOD   = muenblock-client
SERVER_MOD   = muenblock-server
MODULE_FILES = $(SERVER_MOD).ko $(CLIENT_MOD).ko

obj-m = $(MODULE_FILES:.ko=.o)

$(CLIENT_MOD)-objs = common.o client.o
$(SERVER_MOD)-objs = common.o server.o

KERNEL_SOURCE ?= /lib/modules/$(shell uname -r)/build

PWD := $(shell pwd)

all: compile-module
compile-module:
	$(MAKE) -C $(KERNEL_SOURCE) M=$(PWD) modules

install: compile-module
	install -d $(DESTDIR)/lib/modules/extra
	install -m 644 $(MODULE_FILES) $(DESTDIR)/lib/modules/extra

clean:
	$(MAKE) -C $(KERNEL_SOURCE) M=$(PWD) clean
