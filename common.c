// SPDX-License-Identifier: GPL-2.0

/*
 * Muen block I/O device driver.
 *
 * Copyright (C) 2018  secunet Security Networks AG
 * Copyright (C) 2020  secunet Security Networks AG
 *
 */

#include "common.h"
#include "log.h"

#include <linux/device.h>
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/kthread.h>
#include <linux/random.h>
#include <linux/sched/mm.h>
#include <linux/platform_device.h>

void muenblk_cleanup_con(struct muenblk_connection *con, bool server)
{
	struct muenblk_device_channel *reader_chan, *writer_chan;
	struct device *dev = CON_TO_DEV(con);

	if (server) {
		reader_chan = &con->req;
		writer_chan = &con->resp;
		bioset_exit(&con->bio_set);
	} else {
		reader_chan = &con->resp;
		writer_chan = &con->req;
	}

	if (con->reader_thread) {
		kthread_stop(con->reader_thread);
		con->reader_thread = NULL;
	}

	if (con->irq != 0xffffffff)
		devm_free_irq(dev, con->irq, con);

	if (writer_chan->channel_data
			&& writer_chan->channel_element_size > 0) {
		muen_channel_deactivate(writer_chan->channel_data);
		muen_smp_trigger_event(con->trigger_event, con->trigger_cpu);
	}
	writer_chan->channel_element_size = 0;
	reader_chan->channel_element_size = 0;

	if (writer_chan->channel_data) {
		devm_memunmap(dev, writer_chan->channel_data);
		writer_chan->channel_data = NULL;
	}
	if (writer_chan->event) {
		devm_kfree(dev, writer_chan->event);
		writer_chan->event = NULL;
	}

	if (reader_chan->channel_data) {
		devm_memunmap(dev, reader_chan->channel_data);
		reader_chan->channel_data = NULL;
	}
	if (reader_chan->event) {
		devm_kfree(dev, reader_chan->event);
		reader_chan->event = NULL;
	}

	devm_memunmap(dev, con->data_buffer);
	con->data_buffer = NULL;

	dev_dbg(dev, "Channel cleanup successful\n");
}

static irqreturn_t muenblk_irq_handler(int rq, void *c)
{
	struct muenblk_connection *con = c;

	atomic_inc(&con->irq_cnt);
	wake_up(&con->wq);

	return IRQ_HANDLED;
}

static int muenblk_reader_thread(void *c)
{
	struct muenblk_device_channel *chan = c;
	struct muenblk_connection *con = chan->con;
	struct device *dev = CON_TO_DEV(con);
	enum muchannel_reader_result result = MUCHANNEL_SUCCESS;
	int error = 0;
	int irq_cnt = 0;
	int irq_cnt_tmp = 0;

	if (!con->startup_finished)
		wait_event(con->wq, con->startup_finished ||
				kthread_should_stop());

	while (!kthread_should_stop()) {
		wait_event_interruptible(con->wq,
			(irq_cnt != (irq_cnt_tmp = atomic_read(&con->irq_cnt)))
				|| kthread_should_stop());
		if (kthread_should_stop())
			break;

		irq_cnt = irq_cnt_tmp;
read:
		result = muen_channel_read(chan->channel_data,
				&chan->reader,
				chan->event);

		switch (result) {
		case MUCHANNEL_EPOCH_CHANGED:
			dev_dbg(dev, "Channel '%s' received new epoch\n",
					chan->channel_name);
			if (!chan->server) {
				muen_channel_drain(chan->channel_data,
					&chan->reader);
				schedule_work(&con->init_work);
			}
			goto read;

		case MUCHANNEL_SUCCESS:
			error = con->transfer_func(con, chan->event);
			if (unlikely(error))
				dev_err(dev,
					"Channel '%s' processing error %d\n",
					chan->channel_name, error);
			goto read;

		case MUCHANNEL_INACTIVE:
			if (con->disconnect_func) {
				con->disconnect_func(con);
				dev_dbg(dev, "Channel '%s' inactive!",
					chan->channel_name);
			}
			// fall-through
		case MUCHANNEL_NO_DATA:
			break;

		case MUCHANNEL_OVERRUN_DETECTED:
		case MUCHANNEL_INCOMPATIBLE_INTERFACE:
		default:
			dev_err(dev, "Channel '%s' overrun or incompatible interface: %d\n",
					chan->channel_name, result);
			break;
		}
	}
	return 0;
}

int muenblk_init_channel(
	struct muenblk_device_channel *chan,
	struct muenblk_connection *con,
	bool server,
	bool writer)
{
	int error = 0;
	u64 epoch;
	struct device *dev;

	dev = &con->priv->pdev->dev;
	WARN_ON(!dev);
	if (!dev)
		return -1;

	if (!chan) {
		dev_err(dev, "Failed to retrieve channel\n");
		error = -EINVAL;
		goto err_out;
	}
	if (!chan->channel_info->data.mem.address) {
		dev_err(dev, "Failed to retrieve channel information\n");
		error = -EINVAL;
		goto err_out;
	}

	if (chan->channel_info->data.mem.kind != MUEN_MEM_SUBJ_CHANNEL) {
		dev_err(dev, "Not a channel\n");
		error = -EPERM;
		goto err_out;
	}

	if ((server && !writer) || (!server && writer))
		chan->channel_element_size = sizeof(struct muenblk_request);
	else
		chan->channel_element_size = sizeof(struct muenblk_response);

	if (chan->channel_info->data.mem.size < chan->channel_element_size) {
		dev_err(dev, "Channel size of %llu bytes too small, minimum is %lu\n",
		      chan->channel_info->data.mem.size,
		      chan->channel_element_size);
		error = -EINVAL;
		goto err_out;
	}

	if (writer && !(chan->channel_info->data.mem.flags
				& MEM_WRITABLE_FLAG)) {
		dev_err(dev, "Channel is not writeable\n");
		error = -EPERM;
		goto err_out;
	}
	chan->event = devm_kzalloc(dev, chan->channel_element_size, GFP_KERNEL);
	if (!chan->event) {
		error = -ENOMEM;
		goto err_event_out;
	}

	chan->channel_queue_size =
		(chan->channel_info->data.mem.size
		 - sizeof(struct muchannel_header))
		/ chan->channel_element_size;

	if (!devm_request_mem_region(dev, chan->channel_info->data.mem.address,
		chan->channel_info->data.mem.size, "muenblock")) {
		dev_err(dev, "Unable to get Buffer memory region!");
		return -ENOMEM;
	}

	chan->channel_data = devm_memremap(dev,
			chan->channel_info->data.mem.address,
			chan->channel_info->data.mem.size, MEMREMAP_WB);
	if (!chan->channel_data) {
		dev_err(dev, "Failed to allocate channel memory\n");
		error = -ENOMEM;
		goto err_event_out;
	}

	chan->con = con;
	chan->server = server;

	if (writer) {
		get_random_bytes(&epoch, sizeof(epoch));
		muen_channel_init_writer(
				chan->channel_data,
				chan->channel_protocol,
				chan->channel_element_size,
				chan->channel_info->data.mem.size,
				epoch);

	} else {
		enum muchannel_reader_result result = MUCHANNEL_SUCCESS;

		muen_channel_init_reader(&chan->reader, chan->channel_protocol);
		// init reader sets epoch and rc to zero. but the channel might
		// be already initialized and we don't want to read old channel
		// data. A single channel_drain is not enough, since the new
		// epoch is  read with the first channel read and the read pos
		// is reset to zero.
		//
		// -> perform a single read to get the new epoch and then drain
		// the channel.
		result = muen_channel_read(chan->channel_data,
				&chan->reader,
				chan->event);
		if (result == MUCHANNEL_INCOMPATIBLE_INTERFACE) {
			dev_err(dev, "Incompatible interface on channel %s",
					chan->channel_name);
		}
		if (result == MUCHANNEL_EPOCH_CHANGED) {
			muen_channel_drain(chan->channel_data,
				&chan->reader);
			if (!server)
				schedule_work(&con->init_work);
		}
	}

	dev_dbg(dev, "Successfully initialized %s channel (proto 0x%llx, element size %lu, queue size %lu)\n",
			writer ? "writer" : "reader",
			chan->channel_protocol,
			chan->channel_element_size,
			chan->channel_queue_size);

	return 0;

err_event_out:
	chan->event = NULL;

err_out:
	chan->channel_element_size = 0;
	return error;
}

int muenblk_init_connection(struct muenblk_connection *con, bool server)
{
	int error = 0;
	struct device *dev = &con->priv->pdev->dev;
	struct muen_cpu_affinity evt_vec;

	WARN_ON(!con);
	if (!con)
		return -EINVAL;

	con->req.channel_info = (struct muen_resource_type *)
		muen_get_resource(con->req.channel_name, MUEN_RES_MEMORY);
	if (!con->req.channel_info) {
		dev_err(dev, "Failed to determine shm request channel\n");
		return -EINVAL;
	}
	con->resp.channel_info = (struct muen_resource_type *)
		muen_get_resource(con->resp.channel_name, MUEN_RES_MEMORY);
	if (!con->resp.channel_info) {
		dev_err(dev, "Failed to determine shm response channel\n");
		return -EINVAL;
	}

	con->buffer_mem = (struct muen_resource_type *)
		muen_get_resource(con->buffer_mem_name, MUEN_RES_MEMORY);
	if (!con->buffer_mem) {
		dev_err(dev, "Unable to get shared buffer memory!");
		return -EINVAL;
	}
	if (!(con->buffer_mem->data.mem.flags & MEM_WRITABLE_FLAG)) {
		dev_err(dev, "Buffer memory is not writeable!");
		return -EINVAL;
	}

	if (!devm_request_mem_region(dev, con->buffer_mem->data.mem.address,
		con->buffer_mem->data.mem.size, "muenblock")) {
		dev_err(dev, "Unable to get Buffer memory region!");
		return -ENOMEM;
	}

	con->data_buffer = devm_memremap(dev, con->buffer_mem->data.mem.address,
		con->buffer_mem->data.mem.size,
		MEMREMAP_WB);
	if (!con->data_buffer) {
		dev_err(dev, "Failed to allocate buffer memory\n");
		return -ENOMEM;
	}

	spin_lock_init(&con->lock);
	atomic_set(&con->in_flight, 0);
	atomic_set(&con->irq_cnt, 0);
	con->startup_finished = !server;

	if (!muen_smp_one_match(&evt_vec,
			server ? con->resp.channel_name
				: con->req.channel_name,
			MUEN_RES_EVENT)) {
		dev_err(dev, "Unable to get event number for channel %s",
			server ? con->resp.channel_name
				: con->req.channel_name);
		return -EINVAL;
	}

	con->trigger_event = evt_vec.res.data.number;
	con->trigger_cpu = evt_vec.cpu;
	dev_dbg(dev, "Using event %d @ CPU %u on channel %s",
			con->trigger_event, con->trigger_cpu,
			server?con->resp.channel_name:con->req.channel_name);

	if (!muen_smp_one_match(&evt_vec,
			!server ? con->resp.channel_name
				: con->req.channel_name,
			MUEN_RES_VECTOR)) {
		dev_err(dev, "Unable to get event vector for channel %s",
			!server ? con->resp.channel_name
				: con->req.channel_name);
		return -EINVAL;
	}
	con->irq = evt_vec.res.data.number - ISA_IRQ_VECTOR(0);
	error = devm_request_irq(dev, con->irq, muenblk_irq_handler,
			IRQF_ONESHOT, "muen-block", con);
	if (error) {
		dev_err(dev, "Unable to request IRQ: %d error: %d",
			con->irq, error);
		return error;
	}
	dev_dbg(dev, "Using irq %d on channel %s", con->irq,
			!server ? con->resp.channel_name
				: con->req.channel_name);

	error = muenblk_init_channel(&con->req, con, server, !server);
	if (error)
		return error;
	error = muenblk_init_channel(&con->resp, con, server, server);
	if (error) {
		muenblk_cleanup_con(con, server);
		return error;
	}

	con->reader_thread = kthread_run(muenblk_reader_thread,
			!server ? &con->resp : &con->req, "muenblk-reader");
	if (!con->reader_thread)
		return -ENOMEM;

	// wake the other side
	muen_smp_trigger_event(con->trigger_event, con->trigger_cpu);

	dev_dbg(dev, "Successfully initialized shared memory channels\n");
	return 0;
}

int  muenblk_init_priv(
		struct muenblk_priv *priv,
		char *connections_param[MUENBLK_MINOR_COUNT],
		int connections_count,
		transfer_func_t transfer_func,
		disconnect_func_t disconnect_func,
		void (*init_work)(struct work_struct *work),
		bool server)
{
	int error = 0, i, len;
	struct muenblk_connection *con;
	struct device *dev = &priv->pdev->dev;

	atomic_set(&priv->disk_cnt, 0);

	for (i = 0; i < connections_count; i++) {
		enum {CHN_ID, REQ_SHM, RESP_SHM, BUFFER_SHM,
			REQ_PROT, RESP_PROT, LAST};
		char *v[LAST];
		char *c = connections_param[i];
		int j = 0;

		memset(v, '\0', sizeof(v));

		for (j = 0; j < LAST; j++) {
			v[j] = strsep(&c, ":");
			if (v[j] == NULL)
				break;
		}
		con = devm_kzalloc(dev, sizeof(struct muenblk_connection),
				   GFP_KERNEL);
		if (!con)
			return -ENOMEM;

		con->transfer_func = transfer_func;
		con->disconnect_func = disconnect_func;

		init_waitqueue_head(&con->wq);
		init_waitqueue_head(&con->flow_wq);
		idr_init(&con->idr);
		spin_lock_init(&con->idr_lock);

		if (init_work)
			INIT_WORK(&con->init_work, init_work);

		if (!v[CHN_ID]
		    || kstrtou8(v[CHN_ID], 10, &con->id)
		    || (con->id >= MUENBLK_MINOR_COUNT)
		    || (priv->connection_mask & (1<<con->id))) {
			dev_err(dev, "Invalid connection id in %s",
				connections_param[i]);
			error = -EINVAL;
			goto err;
		}

		if (v[REQ_SHM])
			len = strlen(v[REQ_SHM]);

		if (!v[REQ_SHM] || (len  >= 64)) {
			dev_err(dev, "Invalid req_channel in %s",
				connections_param[i]);
			error = -EINVAL;
			goto err;
		}

		strncpy(con->req.channel_name, v[REQ_SHM], len);
		con->req.channel_name[len] = 0;

		if (v[RESP_SHM])
			len = strlen(v[RESP_SHM]);

		if (!v[RESP_SHM] || (len >= 64)) {
			dev_err(dev, "Invalid resp_channel in %s",
				connections_param[i]);
			error = -EINVAL;
			goto err;
		}

		strncpy(con->resp.channel_name, v[RESP_SHM], len);
		con->resp.channel_name[len] = 0;

		if (v[BUFFER_SHM])
			len = strlen(v[BUFFER_SHM]);

		if (!v[BUFFER_SHM] || (len >= 64)) {
			dev_err(dev, "Invalid resp_channel in %s",
				connections_param[i]);
			error = -EINVAL;
			goto err;
		}
		strncpy(con->buffer_mem_name, v[BUFFER_SHM], len);
		con->buffer_mem_name[len] = 0;

		if (v[REQ_PROT] && strlen(v[REQ_PROT]) > 0) {
			error = kstrtoull(v[RESP_PROT], 16,
					&con->req.channel_protocol);
			if (error) {
				dev_err(dev, "Invalid request protocol '%s'\n",
						v[REQ_PROT]);
				error = -EINVAL;
				goto err;
			}
		} else
			con->req.channel_protocol = 0;

		if (v[RESP_PROT] && strlen(v[RESP_PROT]) > 0) {
			error = kstrtoull(v[RESP_PROT], 16,
					&con->resp.channel_protocol);
			if (error) {
				dev_err(dev, "Invalid response protocol '%s'\n",
						v[RESP_PROT]);
				error = -EINVAL;
				goto err;
			}
		} else
			con->resp.channel_protocol = 0;

		con->priv = priv;
		error = muenblk_init_connection(con, server);
		if (error)
			goto err;

		if (server) {
			/* maximum number of bio_vecs = shm_size / PAGE_SIZE */
			error = bioset_init(&con->bio_set,
					(con->buffer_mem->data.mem.size
						>> PAGE_SHIFT),
					0, BIOSET_NEED_BVECS);
			if (error)
				goto err;
		}

		priv->connections[con->id] = con;
		priv->connection_mask |= (1<<con->id);
	}
	return 0;
err:
	devm_kfree(dev, con);
	return error;
}
