// SPDX-License-Identifier: GPL-2.0

/*
 * Muen block I/O device driver.
 *
 * Copyright (C) 2018  secunet Security Networks AG
 * Copyright (C) 2020  secunet Security Networks AG
 *
 */

#include "common.h"
#include "log.h"

#include <linux/bitmap.h>
#include <linux/bitops.h>
#include <linux/count_zeros.h>
#include <linux/init.h>
#include <linux/hdreg.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/major.h>
#include <linux/blkdev.h>
#include <linux/bio.h>
#include <linux/blk-mq.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/sched/mm.h>
#include <linux/fs.h>
#include <linux/cdrom.h>
#include <linux/platform_device.h>
#include <linux/bvec.h>
#include <muen/smp.h>

#define BIO_IDR_START 0x00001000
#define BIO_IDR_END   0xf0000000

// outputs raw bio pages is set to 1
#define DEBUG_MEMCPY  0

static char *connections[MUENBLK_MINOR_COUNT];
static int connections_count;

static void muenblk_client_exit(void);

static void muenblk_client_send_server(struct muenblk_connection *con,
	struct muenblk_request *req)
{
	unsigned long flags;

	spin_lock_irqsave(&con->lock, flags);
	muen_channel_write(con->req.channel_data, req);
	spin_unlock_irqrestore(&con->lock, flags);
	muen_smp_trigger_event(con->trigger_event, con->trigger_cpu);
	REQUEST_TRACE(CON_TO_DEV(con), "send request", req);
}

static uint32_t muenblk_client_cp_segment(struct muenblk_disk *disk,
	struct bio_vec *bvec, unsigned long buffer_offset, bool read)
{
#if DEBUG_MEMCPY
	struct device *dev = CON_TO_DEV(disk->con);
#endif
	uint8_t *page_ptr = NULL;
	uint8_t *data_ptr = NULL;
	uint8_t *buf_ptr = NULL;
	uint32_t page_offset = 0;
	int32_t size = 0;

	page_ptr = kmap_atomic(bvec->bv_page);
	page_offset = bvec->bv_offset;
	data_ptr = page_ptr + page_offset;
	size = bvec->bv_len;

	buf_ptr = (uint8_t *)&disk->con->data_buffer[buffer_offset];

	if (read) {
#if DEBUG_MEMCPY
		COPY_TRACE(dev, "Reading segment data from buffer memory",
			data_ptr, buf_ptr, size);
#endif
		memcpy(data_ptr, buf_ptr, size);
	} else {
#if DEBUG_MEMCPY
		COPY_TRACE(dev, "Writing segment data to buffer memory",
			buf_ptr, data_ptr, size);
#endif
		memcpy(buf_ptr, data_ptr, size);
	}
#if DEBUG_MEMCPY
	print_hex_dump(KERN_INFO, "client ", DUMP_PREFIX_OFFSET, 16, 1,
		buf_ptr, 64, true);
#endif
	kunmap_atomic(page_ptr);
	return size;
}

static int muenblk_client_generic_set_status_pointer(
		struct muenblk_connection *con,
		struct muenblk_response *resp)
{
	struct device *dev = CON_TO_DEV(con);
	uint64_t *p;
	unsigned long flags;

	spin_lock_irqsave(&con->idr_lock, flags);
	p = idr_find(&con->idr, resp->request_tag);
	idr_remove(&con->idr, resp->request_tag);
	spin_unlock_irqrestore(&con->idr_lock, flags);
	if (!p) {
		dev_err(dev, "Request status pointer not found!");
		return -EINVAL;
	}

	spin_lock(&con->flow_wq.lock);
	*p = resp->status_code;
	wake_up_locked(&con->flow_wq);
	spin_unlock(&con->flow_wq.lock);
	return 0;
}

struct rw_data {
	struct request *rq;
	struct muenblk_disk *disk;
	unsigned long start;
	unsigned long sector_cnt;
};

static int muenblk_client_rw_resp(struct muenblk_connection *con,
		struct muenblk_response *resp)
{
	int error = 0;
	struct request *rq;
	struct bio_vec bvec;
	struct req_iterator iter;
	unsigned long flags;
	struct device *dev = CON_TO_DEV(con);
	struct muenblk_disk *disk = con->disks[resp->device_id];
	struct rw_data *rw_data = NULL;
	unsigned int noio_flag = memalloc_noio_save();

	RESPONSE_TRACE(dev, "Processing R/W response", resp);

	spin_lock_irqsave(&con->idr_lock, flags);
	rw_data = idr_find(&con->idr, resp->request_tag);
	idr_remove(&con->idr, resp->request_tag);
	spin_unlock_irqrestore(&con->idr_lock, flags);

	WARN_ON(!rw_data);
	WARN_ON(!disk);

	if (!rw_data || !disk)
		return -EINVAL;

	rq = rw_data->rq;

	dev_dbg(dev, "Request %p finished", rq);

	if (unlikely(resp->status_code)) {
		error = -EIO;
		dev_err(dev, "R/W response include error(s) %lld\n",
			resp->status_code);
		goto finish;
	}

	if (resp->request_kind == MUENBLK_REQUEST_READ) {
		uint64_t start_sec = blk_rq_pos(rq);
		uint64_t offset =
			(rw_data->start << disk->disk_sector_size_shift)
			+ disk->buffer_base;

		rq_for_each_segment(bvec, rq, iter) {
			muenblk_client_cp_segment(disk, &bvec,
				offset
				+ ((iter.iter.bi_sector - start_sec)
					<< disk->disk_sector_size_shift),
				true);
		}
	}
finish:
	blk_mq_end_request(rq, resp->status_code);

	if ((resp->request_kind != MUENBLK_REQUEST_DISCARD) &&
	    (resp->request_kind != MUENBLK_REQUEST_SYNC)) {
		spin_lock_irqsave(&disk->bitmap_lock, flags);
		bitmap_clear(disk->buffer_bitmap, rw_data->start,
				rw_data->sector_cnt);
		spin_unlock_irqrestore(&disk->bitmap_lock, flags);
	}

	spin_lock_irqsave(&disk->queue_lock, flags);
	blk_mq_start_stopped_hw_queues(disk->disk->queue, true);
	spin_unlock_irqrestore(&disk->queue_lock, flags);

	mempool_free(rw_data, &con->priv->metadata_pool);
	memalloc_noio_restore(noio_flag);
	return error;
}

static int muenblk_client_transfer_resp(struct muenblk_connection *con,
	void *r)
{
	struct device *dev = CON_TO_DEV(con);
	struct muenblk_response *resp = (struct muenblk_response *) r;
	int error = 0;

	switch (resp->request_kind) {
	case MUENBLK_REQUEST_READ:
	case MUENBLK_REQUEST_WRITE:
	case MUENBLK_REQUEST_DISCARD:
	case MUENBLK_REQUEST_SYNC:
		muenblk_client_rw_resp(con, resp);
		break;
	case MUENBLK_REQUEST_MEDIA_BLOCKS:
	case MUENBLK_REQUEST_BLOCK_LENGTH:
	case MUENBLK_REQUEST_MAX_DEVICES:
	case MUENBLK_REQUEST_RESET:
		muenblk_client_generic_set_status_pointer(con, resp);
		break;
	default:
		RESPONSE_TRACE(dev, "Unknown response: ", resp);
		break;
	}

	return error;
}

static void muenblk_client_cleanup_disks(struct muenblk_connection *con)
{
	struct muenblk_disk *disk;
	struct device *dev = CON_TO_DEV(con);
	int i;

	for (i = 0; i < MUENBLK_MINOR_COUNT; i++) {
		disk = con->disks[i];
		if (!disk)
			continue;

		if (disk->disk) {
			del_gendisk(disk->disk);
			put_disk(disk->disk);
			blk_mq_free_tag_set(&disk->tag_set);

			disk->disk = NULL;
		}

		if (disk->disk_major) {
			unregister_blkdev(disk->disk_major, disk->disk_name);
			disk->disk_major = 0;
			disk->disk_minor = 0;
		}


		if (disk->buffer_bitmap)
			devm_kfree(dev, disk->buffer_bitmap);

		devm_kfree(dev, disk);
		con->disks[i] = NULL;
	}
}

static void muenblk_client_disconnect(struct muenblk_connection *con)
{
	struct rw_data *rw_data;
	struct muenblk_disk *disk;
	int tag, i;
	unsigned long flags;
	struct device *dev = CON_TO_DEV(con);

	dev_dbg(dev, "Disconnecting..");

	for (i = 0; i < MUENBLK_MINOR_COUNT; i++) {
		disk = con->disks[i];
		if (!disk)
			continue;
		blk_mark_disk_dead(disk->disk);
	}
	// signal io error for all requests in flight
	spin_lock_irqsave(&con->idr_lock, flags);
	idr_for_each_entry(&con->idr, rw_data, tag) {
		if (tag >= BIO_IDR_START && tag < BIO_IDR_END) {
			WARN_ON(!rw_data);
			if (!rw_data)
				break;
			blk_mq_end_request(rw_data->rq, BLK_STS_IOERR);
			spin_lock(&rw_data->disk->bitmap_lock);
			bitmap_clear(rw_data->disk->buffer_bitmap,
					rw_data->start,
					rw_data->sector_cnt);
			spin_unlock(&rw_data->disk->bitmap_lock);

			mempool_free(rw_data, &con->priv->metadata_pool);
		}
		idr_remove(&con->idr, tag);
	}
	spin_unlock_irqrestore(&con->idr_lock, flags);

	for (i = 0; i < MUENBLK_MINOR_COUNT; i++) {
		disk = con->disks[i];
		if (!disk)
			continue;
		spin_lock_irqsave(&disk->queue_lock, flags);
		blk_mq_start_hw_queues(disk->disk->queue);
		spin_unlock_irqrestore(&disk->queue_lock, flags);
	}

	// remove all disks
	muenblk_client_cleanup_disks(con);
}

static int transfer_init(struct muenblk_disk *disk,
		struct muenblk_request *mreq, struct request *rq)
{
	struct rw_data *data;
	unsigned long flags, start = 0;
	int tag, error = 0;
	uint32_t sector_cnt = blk_rq_bytes(rq) >> disk->disk_sector_size_shift;
	struct muenblk_connection *con = disk->con;
	struct device *dev = CON_TO_DEV(con);

	dev_dbg(dev, "%s dir: %d len: %d (%d) op: %d pos: %lld rq: %p\n",
			__func__, rq_data_dir(rq), blk_rq_bytes(rq),
			sector_cnt, req_op(rq), blk_rq_pos(rq), rq);
	// discard is special. Length is set to discard len but there is no data
	if ((req_op(rq) != REQ_OP_DISCARD) && (req_op(rq) != REQ_OP_FLUSH)) {
		if (sector_cnt >= disk->buffer_size) {
			dev_dbg(dev, "Request to large!");
			return -ENOMEM;
		}

		spin_lock_irqsave(&disk->bitmap_lock, flags);
		start = bitmap_find_next_zero_area(disk->buffer_bitmap,
				disk->buffer_size, 0, sector_cnt, 0);
		if (start > disk->buffer_size) {
			spin_unlock_irqrestore(&disk->bitmap_lock, flags);
			dev_dbg(dev, "Full->eagain");
			return -EAGAIN;
		}

		bitmap_set(disk->buffer_bitmap, start, sector_cnt);
		spin_unlock_irqrestore(&disk->bitmap_lock, flags);
	}
	data = mempool_alloc(&con->priv->metadata_pool, GFP_ATOMIC);
	data->start = start;
	data->sector_cnt = sector_cnt;
	data->rq = rq;
	data->disk = disk;

	idr_preload(GFP_ATOMIC);
	spin_lock_irqsave(&con->idr_lock, flags);
	tag = idr_alloc(&con->idr, data, BIO_IDR_START, BIO_IDR_END,
			GFP_ATOMIC);
	spin_unlock_irqrestore(&con->idr_lock, flags);
	idr_preload_end();
	if (tag == -ENOSPC) {
		error = -EAGAIN;
		goto err_cleanup_mempool;
	}

	switch (req_op(rq)) {
	case REQ_OP_READ:
		mreq->request_kind = MUENBLK_REQUEST_READ;
		break;
	case REQ_OP_WRITE:
		mreq->request_kind = MUENBLK_REQUEST_WRITE;
		break;
	case REQ_OP_DISCARD:
		mreq->request_kind = MUENBLK_REQUEST_DISCARD;
		break;
	case REQ_OP_FLUSH:
		mreq->request_kind = MUENBLK_REQUEST_SYNC;
		break;
	default:
		dev_err(dev, "Unsupported request op: %d\n", req_op(rq));
		error = -ENOTSUPP;
		break;
	}

	mreq->device_id		= disk->disk_index;
	mreq->request_tag	= tag;
	mreq->request_length	= blk_rq_bytes(rq);
	mreq->device_offset	= blk_rq_pos(rq);
	mreq->buffer_offset	= (data->start << disk->disk_sector_size_shift)
				  + disk->buffer_base;

	return error;
err_cleanup_mempool:
	mempool_free(data, &con->priv->metadata_pool);

	spin_lock(&disk->bitmap_lock);
	bitmap_clear(disk->buffer_bitmap, start, sector_cnt);
	spin_unlock(&disk->bitmap_lock);

	return error;
}

static blk_status_t muenblk_client_queue_rq(struct blk_mq_hw_ctx *hctx,
		const struct blk_mq_queue_data *bd)
{
	struct bio_vec bvec;
	struct req_iterator iter;
	struct muenblk_request req;
	int ret;
	unsigned long flags;
	struct request *rq = bd->rq;
	struct muenblk_disk *disk = rq->q->queuedata;
	struct device *dev = CON_TO_DEV(disk->con);
	unsigned int noio_flag = memalloc_noio_save();
	blk_status_t blk_sts = BLK_STS_OK;

	blk_mq_start_request(rq);

	if (blk_rq_is_passthrough(rq)) {
		dev_dbg(dev, "Skip non fs request");
		blk_sts = BLK_STS_OK;
		goto out;
	}
	if ((req_op(rq) != REQ_OP_FLUSH) && (blk_rq_bytes(rq) == 0)) {
		dev_dbg(dev, "Received empty request (%d) -> ignoring",
			req_op(rq));
		blk_sts = BLK_STS_OK;
		goto out;
	}

	ret = transfer_init(disk, &req, rq);
	// if there is no space left in shm requeue the request.
	// This is done by signalling BLK_STS_DEV_RESOURCE
	if (ret == -EAGAIN) {
		spin_lock_irqsave(&disk->queue_lock, flags);
		blk_mq_stop_hw_queues(disk->disk->queue);
		spin_unlock_irqrestore(&disk->queue_lock, flags);
		blk_sts = BLK_STS_DEV_RESOURCE;
		goto out;
	}
	if (ret) {
		blk_sts = BLK_STS_IOERR;
		goto out;
	}
	if (req_op(rq) == REQ_OP_WRITE) {
		uint64_t start_sec = blk_rq_pos(rq);

		rq_for_each_segment(bvec, rq, iter) {
			muenblk_client_cp_segment(disk, &bvec,
				req.buffer_offset
				+ ((iter.iter.bi_sector - start_sec)
					<< disk->disk_sector_size_shift),
				false);
		}
	}
	muenblk_client_send_server(disk->con, &req);
out:
	memalloc_noio_restore(noio_flag);
	return blk_sts;
}

static int muenblk_client_getgeo(struct block_device *bdev,
	struct hd_geometry *geo)
{
	struct muenblk_disk *disk = bdev->bd_disk->private_data;

	geo->cylinders =
		((disk->disk_sector_count * disk->disk_sector_size) & ~0x3f)
			>> 6;
	geo->heads = 4;
	geo->sectors = 16;
	geo->start = 4;

	return 0;
}

static int muenblk_client_ioctl(struct block_device *bdev, fmode_t mode,
	unsigned int command, unsigned long arg)
{
	struct muenblk_disk *disk = bdev->bd_disk->private_data;
	struct device *dev = CON_TO_DEV(disk->con);

	switch (command) {
	case CDROM_GET_CAPABILITY:
		/* Used by (busybox) fsck */
		return -ENOTSUPP;
	default:
		dev_dbg(dev, "Received UNKNOWN IOCTL (0x%x)\n", command);
		return -ENOTSUPP;
	}

	return 0;
}

static const struct block_device_operations muenblk_fops = {
	.owner = THIS_MODULE,
	.ioctl = muenblk_client_ioctl,
	.getgeo = muenblk_client_getgeo,
};

static const struct blk_mq_ops muenblock_mq_ops = {
	.queue_rq	= muenblk_client_queue_rq,
};

static int muenblk_client_format_disk_name(char *prefix, int index, char *buf,
	int buflen)
{
	const int base = 'z' - 'a' + 1;
	char *begin = buf + strlen(prefix);
	char *end = buf + buflen;
	char *p;
	int unit;

	p = end - 1;
	*p = '\0';
	unit = base;
	do {
		if (p == begin)
			return -EINVAL;
		*--p = 'a' + (index % unit);
		index = (index / unit) - 1;
	} while (index >= 0);

	memmove(begin, p, end - p);
	memcpy(buf, prefix, strlen(prefix));

	return 0;
}

static int muenblk_client_alloc_disk(struct muenblk_disk *disk)
{
	struct device *dev = CON_TO_DEV(disk->con);
	int error = 0;

	dev_dbg(dev, "Initializing block device...\n");

	if (!disk)
		return -EINVAL;

	error = muenblk_client_format_disk_name(MUENBLK_DEV_NAME,
		disk->disk_index, disk->disk_name, DISK_NAME_LEN);
	if (error) {
		dev_err(dev, "Failed to generate block device disk name\n");
		return error;
	}

	disk->disk_major = register_blkdev(MUENBLK_DEFAULT_MAJOR,
			disk->disk_name);
	if (disk->disk_major <= 0) {
		dev_err(dev, "Failed to register block device\n");
		return -EIO;
	}

	spin_lock_init(&disk->queue_lock);
	disk->tag_set.ops = &muenblock_mq_ops;
	disk->tag_set.nr_hw_queues = 1;
	disk->tag_set.queue_depth = 128;
	disk->tag_set.numa_node = NUMA_NO_NODE;
	disk->tag_set.cmd_size = 0;
	disk->tag_set.flags = BLK_MQ_F_SHOULD_MERGE;
	disk->tag_set.driver_data = disk;

	if (blk_mq_alloc_tag_set(&disk->tag_set)) {
		dev_err(dev, "Failed to allocate mq tag set\n");
		return -ENOMEM;
	}

	disk->disk = blk_mq_alloc_disk(&disk->tag_set, disk);
	if (IS_ERR(disk->disk)) {
		dev_err(dev, "Failed to allocate block device disk\n");
		error = -ENOMEM;
		goto err_blk_cleanup_dev;
	}

	disk->disk->major = disk->disk_major;
	disk->disk->first_minor = MUENBLK_MINOR_FIRST;
	disk->disk->minors =  MUENBLK_MINOR_COUNT;
	disk->disk->private_data = disk;
	disk->disk->flags = 0;
	disk->disk->fops = &muenblk_fops;

	blk_queue_write_cache(disk->disk->queue, true, false);

	disk->disk->queue->limits.discard_granularity = disk->disk_sector_size;
	disk->disk->queue->limits.discard_alignment = disk->disk_sector_size;
	blk_queue_max_discard_sectors(disk->disk->queue, disk->buffer_size);

	memcpy(disk->disk->disk_name, disk->disk_name, DISK_NAME_LEN);
	set_capacity(disk->disk, disk->disk_sector_count);

	dev_dbg(dev, "Successfully initialized block device %s (%u)\n",
		disk->disk_name, disk->disk_index);

	disk->mode = 0;

	return 0;

err_blk_cleanup_dev:
	blk_mq_free_tag_set(&disk->tag_set);
	unregister_blkdev(disk->disk_major, disk->disk_name);
	disk->disk_major = 0;
	disk->disk_minor = 0;
	return error;
}

static int muenblk_client_stop(struct muenblk_priv *priv)
{
	struct device *dev = &priv->pdev->dev;
	struct muenblk_connection *con;
	int i;

	for (i = 0; i < MUENBLK_MINOR_COUNT; i++) {
		con = priv->connections[i];
		if (!con)
			continue;
		muenblk_client_cleanup_disks(con);
		muenblk_cleanup_con(con, false);
	}
	mempool_exit(&priv->metadata_pool);
	devm_kfree(dev, priv);
	dev_info(dev, "stopped!");
	return 0;
}

static int muenblk_client_remove(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct muenblk_priv *priv = dev_get_drvdata(dev);

	return muenblk_client_stop(priv);
}

static int muenblk_client_simple_request(struct muenblk_connection *con,
		uint16_t request_kind,
		uint16_t device_id,
		uint64_t *data)
{
	struct device *dev = CON_TO_DEV(con);
	struct muenblk_request request;
	int error = 0;
	uint32_t id;
	unsigned long flags;

	const uint64_t dummy_data = 0xffffdeadbeefffff;

	memset(&request, 0, sizeof(request));

	idr_preload(GFP_ATOMIC);
	spin_lock_irqsave(&con->idr_lock, flags);
	id = idr_alloc(&con->idr, data, 0, BIO_IDR_START, GFP_ATOMIC);
	spin_unlock_irqrestore(&con->idr_lock, flags);
	idr_preload_end();

	*data = dummy_data;

	request.request_kind = request_kind;
	request.device_id = device_id;
	request.request_tag  = id;

	spin_lock(&con->flow_wq.lock);

	muenblk_client_send_server(con, &request);

	while (1) {
		error = wait_event_interruptible_locked(con->flow_wq,
			*data != dummy_data);

		if (likely(error == 0))
			break;

		if (unlikely(error == -ERESTARTSYS))
			continue;

		if (unlikely(error < 0 || error > 0)) {
			dev_err(dev, "Transfer received signal %d\n",
				error);
			break;
		}
	}
	spin_unlock(&con->flow_wq.lock);

	return error;
}

static void muenblk_client_init_worker(struct work_struct *work)
{
	int error = 0;
	uint64_t cnt = 0;
	int i = 0;
	struct muenblk_disk *disk;
	struct device *dev;
	struct muenblk_priv *priv;
	uint64_t disk_buffer_size;
	struct muenblk_connection *con = container_of(work,
						struct muenblk_connection,
						init_work);

	dev = CON_TO_DEV(con);
	priv = con->priv;
	dev_dbg(dev, "Init worker! %d", irqs_disabled());

	muenblk_client_simple_request(con, MUENBLK_REQUEST_RESET,
				      0, &cnt);

	error = muenblk_client_simple_request(con, MUENBLK_REQUEST_MAX_DEVICES,
					      0, &cnt);
	if (error) {
		dev_err(dev, "Failed to get disk count on connection %d.",
			con->id);
		goto out_fail;
	}
	if ((cnt >= MUENBLK_MINOR_COUNT) || (cnt == 0)) {
		dev_err(dev, "Invalid disk count: %lld", cnt);
		goto out_fail;
	}

	disk_buffer_size = con->buffer_mem->data.mem.size / cnt;

	for (i = 0; i < cnt; i++) {
		disk = devm_kzalloc(dev, sizeof(struct muenblk_disk),
				    GFP_KERNEL);
		if (!disk)
			goto out_fail;

		disk->con = con;
		atomic_set(&disk->bitmap_cnt, 0);

		disk->disk_major = 0;
		disk->disk_minor = 0;
		disk->disk_index = i;
		disk->disk = NULL;

		error = muenblk_client_simple_request(con,
				MUENBLK_REQUEST_MEDIA_BLOCKS,
				i, &disk->disk_sector_count);
		if (error) {
			dev_err(dev, "Failed to retrieve disk size");
			goto out_fail;
		}

		error = muenblk_client_simple_request(con,
				MUENBLK_REQUEST_BLOCK_LENGTH,
				i, &disk->disk_sector_size);
		if (error || (disk->disk_sector_size == 0)) {
			dev_err(dev,
				"Failed to retrieve block len for disk %d", i);
			goto out_fail;
		}

		disk->disk_sector_size_shift =
			count_trailing_zeros(disk->disk_sector_size);

		dev_dbg(dev, "Received disk size (%llu, %llu, %llu, %u)\n",
			disk->disk_sector_count * disk->disk_sector_size,
			disk->disk_sector_count, disk->disk_sector_size,
			disk->disk_sector_size_shift);

		// FIXME: use individual mappings per disk?
		disk->buffer_base = i * disk_buffer_size;

		disk->buffer_size = disk_buffer_size / disk->disk_sector_size;
		if (disk->buffer_size < 1) {
			dev_err(dev, "Buffer size to small!");
			goto out_fail;
		}

		disk->buffer_bitmap = devm_kmalloc_array(&priv->pdev->dev,
				BITS_TO_LONGS(disk->buffer_size),
				sizeof(unsigned long), GFP_KERNEL | __GFP_ZERO);
		if (!disk->buffer_bitmap) {
			dev_err(dev, "Unable to allocate buffer bitmap!");
			goto out_fail;
		}
		spin_lock_init(&disk->bitmap_lock);

		error = muenblk_client_alloc_disk(disk);
		if (error) {
			dev_err(dev, "Failed to allocate disk");
			goto out_fail;
		}

		dev_dbg(dev, "Attaching disk %p to con %p at %d", disk, con, i);
		con->disks[i] = disk;
		error = add_disk(disk->disk);
		if (error)
			dev_err(dev, "Failed to add disk");
	}
out_fail:
	return;
}

static int muenblk_client_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct muenblk_priv *priv;
	int error = 0;

	dev_dbg(dev, "Muenblock client probe!");

	priv = devm_kzalloc(dev, sizeof(struct muenblk_priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	priv->pdev = pdev;

	dev_set_drvdata(dev, priv);

	error = mempool_init_kmalloc_pool(&priv->metadata_pool, 64,
		sizeof(struct rw_data));

	if (error) {
		dev_err(dev, "Mempool init failed %d", error);
		goto err_out;
	}

	error = muenblk_init_priv(priv, connections, connections_count,
			&muenblk_client_transfer_resp,
			&muenblk_client_disconnect,
			muenblk_client_init_worker,
			false);
	if (error) {
		dev_err(dev, "Module init failed %d", error);
		goto err_out;
	}

	dev_info(dev, "Successfully initialized module\n");

	return 0;

err_out:
	muenblk_client_stop(priv);
	return error;
}

static struct platform_driver muenblk_client = {
	.driver = {
		.name = "muenblk_client",
	},
	.probe	= muenblk_client_probe,
	.remove = muenblk_client_remove,
};

/*
 * A release function is mandated by the kernel. Otherwise the following error
 * message will be emitted upon rmmod of the module:
 *   Device 'muenblk_client.0' does not have a release() function, it is
 *   broken and must be fixed.
 */
static void muenblk_client_device_release(struct device *dev)
{

}

static struct platform_device muenblk_platform_device = {
	.name = "muenblk_client",
	.dev = {
		.release = muenblk_client_device_release
	}
};

static int __init muenblk_client_init(void)
{
	int ret;

	ret = platform_driver_register(&muenblk_client);
	if (!ret) {
		ret = platform_device_register(&muenblk_platform_device);
		if (ret)
			platform_driver_unregister(&muenblk_client);
	}
	return ret;
}

static void muenblk_client_exit(void)
{
	platform_device_unregister(&muenblk_platform_device);
	platform_driver_unregister(&muenblk_client);
}

module_init(muenblk_client_init);
module_exit(muenblk_client_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Dennis Wassenberg <dennis.wassenberg@secunet.com>");
MODULE_AUTHOR("Ralf Hubert <ralf.hubert@secunet.com>");
MODULE_DESCRIPTION("Muen SK block device driver (client)");

module_param_array(connections, charp, &connections_count, 0444);
MODULE_PARM_DESC(connections,
	"List of Connections. A connection is defined by a ':' separated list"\
	"of <connection_id>:<req_shm_name>:<resp_shm_name>:<buffer_shm_name>"\
	"and optional <request_shm_protocol>:<response_shm_protocol>.");
