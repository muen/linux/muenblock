/* SPDX-License-Identifier: GPL-2.0 */

/*
 * Muen block I/O device driver.
 *
 * Copyright (C) 2018  secunet Security Networks AG
 * Copyright (C) 2020  secunet Security Networks AG
 *
 */

#ifndef MUENBLK_LOG_H_
#define MUENBLK_LOG_H_

#define BIO_TRACE(dev, p, bio) \
	dev_dbg(dev, "%s - (bio: %u, %u, %llu, %u, %u, %u, %u, %p, %p)\n", \
		p, bio_op(bio), bio->bi_vcnt, \
		bio->bi_iter.bi_sector, bio->bi_iter.bi_size, \
		bio->bi_iter.bi_idx, bio->bi_iter.bi_bvec_done, bio->bi_flags, \
		bio->bi_bdev->bd_disk, bio)

#define REQUEST_TRACE(dev, p, r) \
	dev_dbg(dev, "%s - (request: k: %d, D: %u, T: %u, L: %llu, "\
		     "DOff: %llu, BOff: 0x%llx)", \
		p, r->request_kind, r->device_id, r->request_tag, \
		r->request_length, r->device_offset, r->buffer_offset)

#define RESPONSE_TRACE(dev, p, r) \
	dev_dbg(dev, "%s - (response: k: %d, D: %u, T: %u, S: %llu)", \
		p, r->request_kind, r->device_id, r->request_tag,\
		r->status_code)

#define COPY_TRACE(dev, p, dst, src, size) \
	dev_dbg(dev, "%s - (copy: %p, %p, %llu)\n", p, dst, \
		src, (uint64_t) size)

#endif /* MUENBLK_LOG_H_ */
