// SPDX-License-Identifier: GPL-2.0

/*
 * Muen block I/O device driver.
 *
 * Copyright (C) 2018  secunet Security Networks AG
 * Copyright (C) 2020  secunet Security Networks AG
 *
 */

#include "common.h"
#include "log.h"

#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/hdreg.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/mutex.h>
#include <linux/major.h>
#include <linux/blkdev.h>
#include <linux/bio.h>
#include <linux/gfp.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/blkdev.h>
#include <linux/sched/mm.h>

#include <linux/version.h>

static char *connections[MUENBLK_MINOR_COUNT];
static int connections_count;

static char *devices[MUENBLK_MINOR_COUNT];
static int dev_name_count;

struct muenblk_priv_data {
	struct muenblk_response resp;
	struct muenblk_connection *con;
	uint64_t buffer_offset;
};

static void muenblk_server_send_response(struct muenblk_connection *con,
		struct muenblk_response *resp)
{
	unsigned long flags;

	spin_lock_irqsave(&con->lock, flags);
	muen_channel_write(con->resp.channel_data, resp);
	spin_unlock_irqrestore(&con->lock, flags);
	muen_smp_trigger_event(con->trigger_event, con->trigger_cpu);
}

static void muenblk_send_response_status(struct muenblk_connection *con,
		struct muenblk_request *req, uint64_t status)
{
	struct muenblk_response resp;

	resp.request_kind = req->request_kind;
	resp.request_tag  = req->request_tag;
	resp.device_id    = req->device_id;
	resp.status_code  = status;

	muenblk_server_send_response(con, &resp);
}

static void muenblk_server_cb_endio(struct bio *bio)
{
	struct muenblk_priv_data *pdata;
	struct device *dev;

	WARN_ON(!bio);
	WARN_ON(!bio->bi_private);
	if (!bio || !bio->bi_private)
		return;

	pdata = (struct muenblk_priv_data *)bio->bi_private;
	WARN_ON(!pdata);
	if (!pdata)
		return;
	dev = CON_TO_DEV(pdata->con);

	BIO_TRACE(dev, "Endio bio", bio);

	pdata->resp.status_code = bio->bi_status;

	if (pdata->resp.request_kind == MUENBLK_REQUEST_READ && bio->bi_vcnt) {
		uint8_t *buf_ptr =
			&pdata->con->data_buffer[pdata->buffer_offset];
		uint8_t *bio_ptr =
			(uint8_t *)kmap_atomic(bio->bi_io_vec[0].bv_page);

		memcpy(buf_ptr, bio_ptr, bio->bi_io_vec[0].bv_len);
		kunmap_atomic(bio_ptr);
	}

	if (bio->bi_vcnt)
		__free_pages(bio->bi_io_vec[0].bv_page,
			     get_order(bio->bi_io_vec[0].bv_len));

	if (unlikely(bio->bi_status))
		dev_err(dev, "BIO EndIO invoked with error: %d",
			bio->bi_status);

	bio->bi_private = NULL;
	bio_put(bio);
	muenblk_server_send_response(pdata->con, &pdata->resp);

	atomic_dec(&pdata->con->in_flight);
	wake_up(&pdata->con->wq);

	mempool_free(pdata, &pdata->con->priv->metadata_pool);
}

static void muenblk_process_max_devices_request(struct muenblk_connection *con,
		struct muenblk_request *req)
{
	muenblk_send_response_status(con, req, (uint64_t)con->disks_cnt);
}

static void muenblk_process_media_blocks(struct muenblk_connection *con,
		struct muenblk_request *req)
{
	uint64_t cnt = 0;
	struct muenblk_disk *disk = con->disks[req->device_id];

	if (disk)
		cnt = disk->disk_sector_count;
	muenblk_send_response_status(con, req, cnt);
}

static void muenblk_process_block_len(struct muenblk_connection *con,
		struct muenblk_request *req)
{
	struct muenblk_disk *disk = con->disks[req->device_id];
	uint64_t block_len = 0;

	if (disk)
		block_len =
			queue_logical_block_size(bdev_get_queue(disk->bdev));
	muenblk_send_response_status(con, req, block_len);
}

static void muenblk_process_rw(struct muenblk_connection *con,
		struct muenblk_request *request)
{
	struct bio *bio = NULL;
	struct muenblk_disk *disk = con->disks[request->device_id];
	struct muenblk_priv_data *pdata = NULL;
	struct device *dev = CON_TO_DEV(con);
	struct muenblk_priv *priv = con->priv;
	struct page *pages;

	WARN_ON(!disk);
	if (!disk)
		return;

	REQUEST_TRACE(dev, "Processing request: ", request);
	if (likely((request->request_kind != MUENBLK_REQUEST_DISCARD)
		&& request->request_kind != MUENBLK_REQUEST_SYNC)) {
		if (request->buffer_offset + request->request_length
				> con->buffer_mem->data.mem.size) {
			dev_err(dev, "Invalid buffer offset: %llx",
				request->buffer_offset);
			goto exit;
		}

		bio = bio_alloc_bioset(disk->bdev, 1, 0, GFP_ATOMIC, &con->bio_set);
		if (IS_ERR(bio)) {
			dev_err(dev, "Unable to map bio! %ld", PTR_ERR(bio));
			goto exit;
		}
		pages = alloc_pages(GFP_ATOMIC,
				    get_order(request->request_length));
		if (!pages)
			goto exit_bio;

		bio_add_page(bio, pages, request->request_length, 0);

	} else {
		bio = bio_alloc(disk->bdev, 0, 0, GFP_KERNEL);
	}

	if (IS_ERR(bio)) {
		dev_err(dev, "Unable to map bio! %ld", PTR_ERR(bio));
		goto exit;
	}

	pdata = mempool_alloc(&priv->metadata_pool, GFP_KERNEL);
	bio->bi_private = pdata;

	bio->bi_end_io = muenblk_server_cb_endio;
	bio->bi_iter.bi_sector = request->device_offset;

	switch (request->request_kind) {
	case MUENBLK_REQUEST_READ:
		bio->bi_opf = REQ_OP_READ;
		break;
	case MUENBLK_REQUEST_WRITE: {
		uint8_t *buf_ptr = &con->data_buffer[request->buffer_offset];

		memcpy(page_address(pages), buf_ptr, request->request_length);
		bio->bi_opf = REQ_OP_WRITE;
		}
		break;
	case MUENBLK_REQUEST_DISCARD:
		bio->bi_opf = REQ_OP_DISCARD;
		break;
	case MUENBLK_REQUEST_SYNC:
		bio->bi_opf = REQ_OP_FLUSH | REQ_PREFLUSH;
		break;
	}

	if (!bio_has_data(bio))
		bio->bi_iter.bi_size = request->request_length;

	pdata->resp.request_kind = request->request_kind;
	pdata->resp.request_tag = request->request_tag;
	pdata->resp.device_id = request->device_id;
	pdata->buffer_offset  = request->buffer_offset;
	pdata->con = con;

	atomic_inc(&pdata->con->in_flight);
	BIO_TRACE(dev, "Generate block request for bio", bio);
	submit_bio_noacct(bio);
	return;

exit_bio:
	bio_put(bio);
exit:
	pdata->resp.status_code = EIO;
	muenblk_server_send_response(con, &pdata->resp);
}

static void muenblk_process_reset(struct muenblk_connection *con,
		struct muenblk_request *req)
{
	// wait until all disk requests are done
	while (wait_event_interruptible(con->wq,
				atomic_read(&con->in_flight) == 0) != 0)
		;

	// send response
	muenblk_send_response_status(con, req, 0);
}

static int muenblk_server_transfer_req(struct muenblk_connection *con,
	void *request)
{
	struct muenblk_request *req = (struct muenblk_request *) request;
	struct device *dev = CON_TO_DEV(con);
	unsigned int flags = memalloc_noio_save();

	WARN_ON(!req);

	if (!req || (req->device_id >= MUENBLK_MINOR_COUNT))
		return -EINVAL;

	switch (req->request_kind) {
	case MUENBLK_REQUEST_READ:
	case MUENBLK_REQUEST_WRITE:
	case MUENBLK_REQUEST_DISCARD:
	case MUENBLK_REQUEST_SYNC:
		muenblk_process_rw(con, req);
		break;
	case MUENBLK_REQUEST_MEDIA_BLOCKS:
		muenblk_process_media_blocks(con, req);
		break;
	case MUENBLK_REQUEST_BLOCK_LENGTH:
		muenblk_process_block_len(con, req);
		break;
	case MUENBLK_REQUEST_MAX_DEVICES:
		muenblk_process_max_devices_request(con, req);
		break;
	case MUENBLK_REQUEST_RESET:
		muenblk_process_reset(con, req);
		break;
	default:
		dev_err(dev, "Unknown request received!");
		break;
	}
	memalloc_noio_restore(flags);
	return 0;
}

static int muenblk_server_init_block_device(struct device *dev,
		struct muenblk_disk *disk)
{
	int error = 0;
	fmode_t mode = FMODE_READ | FMODE_WRITE | FMODE_LSEEK;

	struct request_queue *queue;
	struct block_device *bdev;

	bdev = blkdev_get_by_path(disk->disk_name, mode, disk);
	if (IS_ERR(bdev)) {
		dev_err(dev,
			"Failed to retrieve selected block device '%s'",
			disk->disk_name);
		error = PTR_ERR(bdev);
		goto exit;
	}

	disk->bdev = bdev;
	disk->disk = bdev->bd_disk;
	disk->mode = mode;

	queue = bdev_get_queue(bdev);

	disk->disk_sector_count = bdev_nr_sectors(bdev);
	disk->disk_sector_size  = queue_logical_block_size(queue);

	blk_queue_max_hw_sectors(queue,
			disk->con->buffer_mem->data.mem.size
				/ disk->disk_sector_size);

	dev_info(dev,
		"Successfully initialized block device %s (%llu, %llu, %llu)",
		disk->disk_name,
		disk->disk_sector_count * disk->disk_sector_size,
		disk->disk_sector_count,
		bdev->bd_start_sect);

exit:
	return error;
}

/*
 * A release function is mandated by the kernel. Otherwise the following error
 * message will be emitted upon rmmod of the module:
 *   Device 'muenblk_server.0' does not have a release() function, it is
 *   broken and must be fixed.
 */
static void muenblk_server_dev_release(struct device *dev)
{

}

static int muenblk_server_remove(struct platform_device *pdev)
{
	int i, j;
	struct muenblk_connection *con;
	struct muenblk_disk *disk;
	struct device *dev = &pdev->dev;
	struct muenblk_priv *priv = dev_get_drvdata(dev);

	for (i = 0; i < MUENBLK_MINOR_COUNT; i++) {
		con = priv->connections[i];
		if (!con)
			continue;
		// halt request thread -> no more disk requests possible
		kthread_stop(con->reader_thread);
		con->reader_thread = NULL;

		// wait until all disk requests are handled
		while (wait_event_interruptible(con->wq,
				atomic_read(&con->in_flight) == 0) != 0)
			;
		for (j = 0; j < MUENBLK_MINOR_COUNT; j++) {
			disk = con->disks[j];
			if (!disk)
				continue;

			if (disk->disk) {
				put_disk(disk->disk);
				disk->disk = NULL;
			}
			if (disk->bdev) {
				blkdev_put(disk->bdev, disk->mode);
				disk->bdev = NULL;
			}
			devm_kfree(dev, disk);
		}
		muenblk_cleanup_con(con, true);
	}
	mempool_exit(&priv->metadata_pool);

	devm_kfree(dev, priv);
	dev_info(dev, "removed");
	return 0;
}

static int muenblk_server_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct muenblk_priv *priv;
	struct muenblk_connection *con;
	int i = 0;
	unsigned int len = 0;
	int error = 0;

	if (!dev_name_count || !connections_count
	    || (dev_name_count >= 16)
	    || (connections_count >= 16)) {
		dev_err(dev, "Invalid number of connections or devices!");
		return -EINVAL;
	}

	priv = devm_kzalloc(dev, sizeof(struct muenblk_priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	priv->pdev = pdev;
	dev_set_drvdata(dev, priv);

	error = muenblk_init_priv(priv, connections, connections_count,
			muenblk_server_transfer_req,
			NULL, NULL, true);
	if (error)
		return error;

	error = mempool_init_kmalloc_pool(&priv->metadata_pool, 256,
		sizeof(struct muenblk_priv_data));
	if (error) {
		dev_err(dev, "Failed to create metadata pool: %d\n", error);
		return error;
	}


	for (i = 0; i < dev_name_count; i++) {
		struct muenblk_disk *disk;
		enum {NAME, CHN_ID, LAST};
		char *v[LAST];
		char *c = devices[i];
		int j = 0;

		memset(v, '\0', sizeof(v));

		for (j = 0; j < LAST; j++) {
			v[j] = strsep(&c, ":");
			if (v[j] == NULL)
				break;
		}

		disk = devm_kzalloc(dev, sizeof(struct muenblk_disk),
			GFP_KERNEL);
		if (!disk) {
			error = -ENOMEM;
			goto out;
		}

		disk->disk_major = 0;
		disk->disk_minor = 0;

		if (v[NAME])
			len = strlen(v[NAME]);

		if (!v[NAME] || (len >= 64)) {
			error = -EINVAL;
			goto out;
		}

		strncpy(disk->disk_name, v[NAME], len);
		disk->disk_name[len] = 0;

		if (!v[CHN_ID]
		    || kstrtou8(v[CHN_ID], 10, &disk->con_id)
		    || (disk->con_id >= MUENBLK_MINOR_COUNT)) {
			dev_err(dev, "Invalid connection id in %s", devices[i]);
			error = -EINVAL;
			goto out;
		}

		con = priv->connections[disk->con_id];
		if (!con) {
			dev_err(dev, "No connection for connection ID %d!",
				disk->con_id);
			error = -EINVAL;
			goto out;
		}
		disk->con = con;
		disk->disk_index = con->disks_cnt++;
		con->disks[disk->disk_index] = disk;

		error = muenblk_server_init_block_device(dev, disk);
		if (error)
			goto out;

		con->startup_finished = true;
		wake_up(&con->wq);
	}
	dev_info(dev, "Successfully initialized module\n");
	return 0;
out:
	muenblk_server_remove(pdev);
	return error;
}

static struct platform_driver muenblk_server = {
	.driver = {
		.name = "muenblk_server",
	},
	.probe	= muenblk_server_probe,
	.remove = muenblk_server_remove,
};

static struct platform_device muenblk_platform_device = {
	.name = "muenblk_server",
	.dev = {
		.release = muenblk_server_dev_release,
	},
};

static int __init muenblk_server_init(void)
{
	int ret;

	ret = platform_driver_register(&muenblk_server);
	if (!ret) {
		ret = platform_device_register(&muenblk_platform_device);
		if (ret)
			platform_driver_unregister(&muenblk_server);
	}
	return ret;
}

static void muenblk_server_exit(void)
{
	platform_device_unregister(&muenblk_platform_device);
	platform_driver_unregister(&muenblk_server);
}

module_init(muenblk_server_init);
module_exit(muenblk_server_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Dennis Wassenberg <dennis.wassenberg@secunet.com>");
MODULE_AUTHOR("Ralf Hubert <ralf.hubert@secunet.com>");
MODULE_DESCRIPTION("Muen SK block device driver (server)");

module_param_array(connections, charp, &connections_count, 0444);
MODULE_PARM_DESC(connections,
	"List of Connections. A connection is defined by a ':' separated list \
	of \
	<connection_id>:<req_shm_name>:<resp_shm_name>:<buffer_shm_name> \
	and optional <request_shm_protocol>:<response_shm_protocol>.");

module_param_array(devices, charp, &dev_name_count, 0444);
MODULE_PARM_DESC(devices, "List of block devices. \
	<blockdev>:<channel_id>");
