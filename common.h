/* SPDX-License-Identifier: GPL-2.0 */

/*
 * Muen block I/O device driver.
 *
 * Copyright (C) 2018  secunet Security Networks AG
 * Copyright (C) 2020  secunet Security Networks AG
 *
 */

#ifndef MUENBLK_COMMON_H_
#define MUENBLK_COMMON_H_

// #define DEBUG

#define MUENBLK_DEFAULT_MAJOR	0
#define MUENBLK_MINOR_FIRST	0
#define MUENBLK_MINOR_COUNT	16
#define MUENBLK_DEV_NAME	"muendisk"

#include <linux/device.h>
#include <linux/bio.h>
#include <linux/blk-mq.h>
#include <linux/idr.h>
#include <linux/workqueue.h>
#include <linux/semaphore.h>

#include <muen/sinfo.h>
#include <muen/reader.h>
#include <muen/writer.h>
#include <muen/smp.h>

/* Request Kinds */
#define MUENBLK_REQUEST_NONE	0
#define MUENBLK_REQUEST_READ	1
#define MUENBLK_REQUEST_WRITE	2
#define MUENBLK_REQUEST_DISCARD	3
#define MUENBLK_REQUEST_SYNC	4

/* Return number of Blocks of device */
#define MUENBLK_REQUEST_MEDIA_BLOCKS 0x10
/* Return block size of device */
#define MUENBLK_REQUEST_BLOCK_LENGTH 0x11
/* Return maximum number of requests in flight */
#define MUENBLK_REQUEST_MAX_REQUESTS 0x19
/* Return max number of blocks per read/write/discard request */
#define MUENBLK_REQUEST_MAX_BLOCKS_COUNT 0x1a
/* Return number of devices */
#define MUENBLK_REQUEST_MAX_DEVICES 0x1b
/* Assure that no requests are pending upon return */
#define MUENBLK_REQUEST_RESET 0x1f

struct muenblk_request {
	uint16_t	request_kind;
	/// Device (0 .. Max_Devices - 1)
	uint16_t	device_id;
	/// Chosen by client; client SHOULD avoid duplicate tags in flight
	uint32_t	request_tag;
	/// Number of Bytes to be handled (1 .. Max_Length)
	/// Zero for None, must be a multiple of Block_Length
	uint64_t	request_length;
	/// Operation affects Device Byte Address
	///  (Device_Offset..Device_Offset + Request_Length-1)
	/// Zero for None, must be aligned to Block_Length
	uint64_t	device_offset;
	/// Byte offset into DMA Buffer
	///  DMA Buffer used for operation:
	///    (Buffer_Offset .. Buffer_Offset + Request_Length -1)
	/// Zero for None, must be aligned to Block_Length
	uint64_t	buffer_offset;
};

struct muenblk_response {
	uint16_t	request_kind;
	/// Device (0 .. Max_Devices - 1)
	uint16_t	device_id;
	/// Copied from corresponding request
	uint32_t	request_tag;
	uint64_t	status_code;
};


struct muenblk_priv;
struct muenblk_connection;

typedef int(*transfer_func_t)(struct muenblk_connection *con, void *event);
typedef void(*disconnect_func_t)(struct muenblk_connection *con);

struct muenblk_device_channel {
	struct muenblk_connection	*con;
	char				channel_name[64];
	struct muen_resource_type	*channel_info;

	struct muchannel		*channel_data;
	struct muchannel_reader		reader;

	size_t				channel_element_size;
	size_t				channel_queue_size;
	uint64_t			channel_protocol;


	void				*event; // either request or response

	bool				server;
};

struct muenblk_disk {
	char				disk_name[DISK_NAME_LEN];
	struct gendisk			*disk;
	struct block_device		*bdev;
	struct muenblk_connection	*con;

	uint32_t			buffer_size;
	uint32_t			buffer_base;

	spinlock_t			bitmap_lock;
	unsigned long			*buffer_bitmap;
	atomic_t			bitmap_cnt;

	uint64_t			disk_sector_count;
	uint64_t			disk_sector_size;
	uint32_t			disk_sector_size_shift;
	uint32_t			disk_major;
	uint8_t				disk_minor;
	uint8_t				disk_index;
	uint8_t				con_id;

	spinlock_t			queue_lock;
	fmode_t				mode;

	struct				blk_mq_tag_set tag_set;
};

struct muenblk_connection {
	struct muenblk_priv		*priv;
	struct muenblk_device_channel	req;
	struct muenblk_device_channel	resp;

	spinlock_t			lock;

	char				buffer_mem_name[DISK_NAME_LEN];
	struct muen_resource_type	*buffer_mem;
	uint8_t				*data_buffer;

	uint8_t				disks_cnt;
	struct muenblk_disk		*disks[MUENBLK_MINOR_COUNT];

	uint8_t				id;

	struct work_struct		init_work;
	wait_queue_head_t               wq;
	wait_queue_head_t		flow_wq;

	int				irq;

	uint8_t				trigger_event;
	uint8_t				trigger_cpu;

	struct task_struct		*reader_thread;
	transfer_func_t			transfer_func;
	disconnect_func_t		disconnect_func;

	atomic_t			irq_cnt;
	uint32_t			irq_handled;

	struct idr			idr;
	spinlock_t			idr_lock;

	atomic_t			in_flight;
	bool				startup_finished;

	struct bio_set			bio_set;
	mempool_t			page_pool;
};

struct muenblk_priv {
	struct platform_device		*pdev;

	mempool_t			metadata_pool;

	uint16_t			connection_mask;

	struct muenblk_connection	*connections[MUENBLK_MINOR_COUNT];

	atomic_t			disk_cnt;
};

#define CON_TO_PRIV(con)	(con->priv)
#define CON_TO_DEV(con)		(&con->priv->pdev->dev)

void muenblk_cleanup_con(struct muenblk_connection *con, bool server);

int muenblk_init_priv(
		struct muenblk_priv *priv,
		char *connections_param[MUENBLK_MINOR_COUNT],
		int connections_count,
		transfer_func_t transfer_func,
		disconnect_func_t disconnect_func,
		void (*init_work)(struct work_struct *work),
		bool server);

#endif /* MUENBLK_COMMON_H_ */
