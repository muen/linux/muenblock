# Muenblock

## Introduction

The *muenblock* Linux kernel module implements a block device driver which
transports block I/O requests (bio) via shared memory channels provided by the
[Muen Separation Kernel][1].

The server part enables the export of entire block devices or single
partitions from a Linux subject to the driver's client part running on another
Linux subject.

Note: The *muenblock* kernel modules are in an experimental state and not yet
ready for production use.

## Usage

The following command inserts the *muenblock-server* module into the kernel on
the server side. It exports the complete `sda` disk via the *blockdev_request1*
and *blockdev_response1* channels to the client module.

    $ modprobe muenblock-server \
		connections=1:blockdev_request1:blockdev_response1:blockdev_mem \
		device=/dev/sda:1

The connections parameter is defined as a comma separated list of connection
strings with the fields:

	<connection_id>:<request_shm_name>:<response_shm_name>:<buffer_shm_name>

 and optional

	<request_shm_protocol>:<response_shm_protocol>

Where `connection_id` is used to map a device to a connection. It is possible to
have multiple devices on a single connection.
`*_shm_name` are reader and writer channels. `buffer_shm_name` is used as plain
shared memory.
The reader and writer protocols are arbitrary values which must match between
communicating endpoints.

The device parameter is a comma separated list of device to connection mappings:

	<block device>:<connection_id>

Where block_device is a existing block device on the server and `connection_id`
must match a `connection_id` of the connections parameter.

The following command is then executed in the client virtual machine to insert
the *muenblock-client* module:

	$ modprobe muenblock-client \
		connections=1:blockdev_request1:blockdev_response1:muenblock

The client only gets the connections parameter which is defined in the same way
as for the server.

This leads to the creation of a */dev/muendiska* block device on the client
which can be used like a directly assigned disk.

It is also possible to only export specific partitions of a block device, see
the following two example commands on the server and client respectively:

	$ depmod muenblock-server \
		connections=1:blockdev_request1:blockdev_response1:blockdev_mem \
		device=/dev/sda1:1,/dev/sdb5:1

	$ depmod muenblock-client \
		connections=1:blockdev_request1:blockdev_response1:blockdev_mem

Each partition will appear as separate *muendiska* and *muendiskb* devices in
the client.

## I/O Scheduler Performance

To increase performance, set the I/O scheduler of the block device exported by
the *muenblock* server module to `noop` (or `none`, depending on the kernel
version):

 # echo noop > /sys/class/block/sda/queue/scheduler

## Supported Filesystems

Filesystems of the ext\* family have been extensively tested, also in layered
modes (ext filesystem on dm-crypt, LVM or both) and are known to work.

[1]: https://muen.sk
